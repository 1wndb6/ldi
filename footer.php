<?php
$footer = get_field('footer', 'options');
$logos = $footer['logos'];
$text_background = $footer['text_background'];
$text = $footer['text'];
$copyright = $footer['copyright'];
?>
<footer class="footer">
    <div class="footer__wrap">
        <?php if ($logos) { ?>
            <div class="footer__logos">
                <?php foreach ($logos as $logo) { ?>
                    <img src="<?php echo $logo['url'] ?>" alt="<?php echo $logo['alt'] ?>" class="item">
                <?php } ?>
            </div>
        <?php } ?>
        <?php if ($text || $copyright) { ?>
            <div class="footer__bottom" style="background-image: url(<?php echo $text_background ?>)">
                <?php if ($text) { ?>
                    <p class="footer__text"><?php echo $text ?></p>
                <?php } ?>
                <?php if ($copyright) { ?>
                    <p class="footer__copy"><?php echo $copyright ?></p>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>