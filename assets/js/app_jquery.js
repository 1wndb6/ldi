var $ = jQuery;
$(document).ready(function () {
    var mobile = window.matchMedia('(min-width: 0px) and (max-width: 768px)');
    let scrollTop = $(window).scrollTop(),
        lastScrollTop = scrollTop;
    if (mobile.matches && scrollTop > 200) {
        $("header").addClass("header--scrolled");
    } else if (scrollTop > 800) {
        $("header").addClass("header--scrolled");
    }

    function throttle(fn, wait) {
        var time = Date.now();
        return function() {
            if ((time + wait - Date.now()) < 0) {
                fn();
                time = Date.now();
            }
        }
    }

    window.addEventListener('scroll', throttle(DocumentScroll, 100));

    function DocumentScroll() {
        scrollTop = $(window).scrollTop();

        if($('header').hasClass('autoHide')) {
            if (scrollTop < (lastScrollTop - 20)) {
                // scroll UP
                $('header').removeClass("header--hide");
            } else {
                // scroll DOWN
                if (scrollTop > $('header').height()) {
                    $('header').addClass("header--hide");
                }
            }
        }

        if (mobile.matches) {
            (scrollTop > 200) ? $('header').addClass("header--scrolled") : $('header').removeClass("header--scrolled");
        } else {
            (scrollTop > 800) ? $('header').addClass("header--scrolled") : $('header').removeClass("header--scrolled");
        }

        lastScrollTop = scrollTop;
    }

    //Hidden Menu

    $('.menu-header .menu-item-has-children a').click(function () {
        $(this).siblings('.sub-menu').slideToggle(200)
    })

    $('.header__burger').on('click', function () {
        if (!$('body').hasClass('menu-open')) {
            $('body').addClass('menu-open');
        } else {
            $('body').removeClass('menu-open');
        }
    });

    $('.menu-item a').on('click', function () {
        $('.header__burger').removeClass('active');
        $('body').removeClass('menu-open');
    });

});
