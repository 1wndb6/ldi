document.addEventListener('DOMContentLoaded', function () {
    (() => {
        var menu = document.querySelector('.header__menu'),
            menuBtn = document.querySelector('.header__menu__btn')
        menuBtn.addEventListener('click', function () {
            this.classList.toggle('open')
            menu.classList.toggle('open')
        })
    })();
})