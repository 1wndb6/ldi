<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Asap:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Asap:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Climate+Crisis&display=swap"
        rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="header">
    <div class="container">
        <div class="header__wrap">
            <div class="header__logo">
                <?php if (function_exists('the_custom_logo') && has_custom_logo()) the_custom_logo(); ?>
            </div>
            <div class="header__menu__btn">
                <?php
                echo file_get_contents(THEME_URI . '/dest/img/menu-close.svg');
                echo file_get_contents(THEME_URI . '/dest/img/menu-open.svg');
                ?>
            </div>
            <nav class="header__menu">
                <?php wp_nav_menu(['menu' => 'header_menu']) ?>
            </nav>
        </div>
    </div>
</header>
