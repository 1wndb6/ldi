<?php get_header(); ?>
    <main class="page-default">
        <?php while (have_posts()) {
            the_post();
            the_content();
        } ?>
    </main>
<?php get_footer();