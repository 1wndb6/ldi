<?php
/*
Template Name: 404
*/
get_header(); ?>
    <main class="page-error">
        <section class="error">
            <div class="error__wrap">
                <h1><?php esc_html_e('404 Not Found', 'THEME_NAME') ?></h1>
                <p><?php esc_html_e('It looks like nothing was found at this location.', 'THEME_NAME'); ?></p>
            </div>
        </section>
    </main>
<?php get_footer();