<?php

add_action('acf/init', function () {
    if (function_exists('acf_add_options_page')) {
        acf_add_options_page([
            'page_title' => __('Theme General Settings', 'ldi'),
            'menu_title' => __('Theme Settings', 'ldi'),
            'menu_slug'  => 'theme-general-settings',
            'capability' => 'edit_posts',
            'redirect'   => false,
        ]);
    }

    if (function_exists('acf_register_block_type')) {
        $blocks = [
            'banner'      => ['title' => 'Banner', 'render' => 'banner', 'style' => 'banner'],
            'rates'       => ['title' => 'Rates', 'render' => 'rates', 'style' => 'rates'],
            'image_texts' => ['title' => 'Image & Texts', 'render' => 'image-texts', 'style' => 'image-texts'],
            'image_text'  => ['title' => 'Image & Text', 'render' => 'image-text', 'style' => 'image-text'],
            'title_text'  => ['title' => 'Title & Text', 'render' => 'title-text', 'style' => 'title-text'],
            'title_list'  => ['title' => 'Title & List', 'render' => 'title-list', 'style' => 'title-list'],
        ];
        foreach ($blocks as $key => $block) {
            acf_register_block_type([
                'name'            => $key,
                'title'           => __($block['title'], 'ldi'),
                'description'     => __('', 'ldi'),
                'category'        => 'acf_fx_blocks',
                'icon'            => 'slides',
                'keywords'        => ['image', 'title', 'text'],
                'mode'            => 'edit',
                'render_template' => THEME_PATH . '/templates/parts/blocks/' . $block['render'] . '.php',
                'enqueue_style'   => THEME_URI . '/dest/css/main/blocks/' . $block['style'] . '.min.css',
            ]);
        }
    }
});