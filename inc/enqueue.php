<?php
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('app-css', THEME_URI . '/dest/css/main/app.min.css');

    wp_enqueue_script('app-js', THEME_URI . '/dest/js/app.min.js', ['jquery'], time(), true);

    wp_localize_script('app-js', 'argo_ajax',
        [
            'url'       => admin_url('admin-ajax.php'),
            'theme_uri' => THEME_URI,
        ]
    );
});