<?php
$fields = get_fields();
$title = $fields['title'];
$list = $fields['list'];
if (!$list) {
    return;
}
?>
<section class="title_list">
    <div class="container">
        <div class="title_list__wrap">
            <?php if ($title) { ?>
                <p class="title_list__title"><?php echo $title ?></p>
            <?php } ?>
            <div class="title_list__list">
                <?php foreach ($list as $item) {
                    if (!$item['item']) {
                        continue;
                    } ?>
                    <div class="item">
                        <span></span>
                        <p><?php echo $item['item'] ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>