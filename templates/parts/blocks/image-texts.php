<?php
$fields = get_fields();
$image = $fields['image'];
$texts = $fields['texts'];
if (!$texts) {
    return;
}
?>
<section class="image_texts">
    <div class="container">
        <div class="image_texts__wrap">
            <?php if ($image) { ?>
                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>" class="image_texts__img">
            <?php }
            foreach ($texts as $text) {
                if (!$text['text']) {
                    continue;
                } ?>
                <p class="image_texts__item"><?php echo $text['text'] ?></p>
            <?php } ?>
        </div>
    </div>
</section>
