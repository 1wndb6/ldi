<?php
$fields = get_fields();
$image = $fields['image'];
$title = $fields['title'];
$text = $fields['text'];
?>
<section class="image_text">
    <div class="container">
        <div class="image_text__wrap">
            <?php if ($image) { ?>
                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>" class="image_text__img">
            <?php }
            if ($title) { ?>
                <p class="image_text__title"><?php echo $title ?></p>
            <?php }
            if ($text) { ?>
                <p class="image_text__text"><?php echo $text ?></p>
            <?php } ?>
        </div>
    </div>
</section>