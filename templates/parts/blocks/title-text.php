<?php
$fields = get_fields();
$title_one = $fields['title_one'];
$title_two = $fields['title_two'];
$text = $fields['text'];
?>
<section class="title_text">
    <div class="container">
        <div class="title_text__wrap">
            <?php if ($title_one || $title_two) { ?>
                <div class="title_text__title">
                    <?php if ($title_one) { ?>
                        <p class="title_text__title_one"><?php echo $title_one ?></p>
                    <?php }
                    if ($title_two) { ?>
                        <p class="title_text__title_two"><?php echo $title_two ?></p>
                    <?php } ?>
                </div>
            <?php }
            if ($text) { ?>
                <p class="title_text__text"><?php echo $text ?></p>
            <?php } ?>
        </div>
    </div>
</section>