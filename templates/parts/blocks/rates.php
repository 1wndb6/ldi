<?php
$fields = get_fields();
$rates = $fields['rates'];
if (!$rates) {
    return;
}
?>
<section class="rates">
    <div class="container">
        <div class="rates__wrap">
            <?php foreach ($rates as $rate) {
                $logo = $rate['logo'];
                $title = $rate['title'];
                $price = $rate['price'];
                $bonus = $rate['bonus'];
                $text = $rate['text'];
                $button = $rate['button'];
                ?>
                <div class="rate_item">
                    <div class="rate_item__top">
                        <div class="rate_item__top__left">
                            <?php if ($logo) { ?>
                                <img src="<?php echo $logo['url'] ?>" alt="<?php echo $logo['alt'] ?>" class="rate_item__logo">
                            <?php }
                            if ($title) { ?>
                                <p class="rate_item__title"><?php echo $title ?></p>
                            <?php } ?>
                        </div>
                        <div class="rate_item__top__right">
                            <?php if ($price) { ?>
                                <p class="rate_item__price"><?php echo $price ?></p>
                            <?php }
                            if ($bonus) { ?>
                                <p class="rate_item__bonus"><?php echo $bonus ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="rate_item__bottom">
                        <div class="rate_item__bottom__left">
                            <?php if ($text) { ?>
                                <p class="rate_item__text"><?php echo $text ?></p>
                            <?php } ?>
                        </div>
                        <div class="rate_item__bottom__right">
                            <?php if ($button['url'] && $button['title']) { ?>
                                <a href="<?php echo $button['url'] ?>" target="<?php echo $button['target'] ?>" class="rate_item__btn btn_main">
                                    <?php echo $button['title'] ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>