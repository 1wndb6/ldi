<?php
$fields = get_fields();
$title_one = $fields['title_one'];
$title_two = $fields['title_two'];
$subtitle = $fields['subtitle'];
$left_text = $fields['left_text'];
$right_text = $fields['right_text'];
?>
<section class="banner">
    <div class="container">
        <div class="banner__wrap">
            <?php if ($title_one || $title_two) { ?>
                <div class="banner__title">
                    <?php if ($title_one) { ?>
                        <p class="banner__title_one"><?php echo $title_one ?></p>
                    <?php }
                    if ($title_two) { ?>
                        <p class="banner__title_two"><?php echo $title_two ?></p>
                    <?php } ?>
                </div>
            <?php }
            if ($subtitle) { ?>
                <p class="banner__subtitle"><?php echo $subtitle ?></p>
            <?php }
            if ($left_text || $right_text) { ?>
                <div class="banner__bottom">
                    <?php if ($left_text) { ?>
                        <p class="banner__bottom__left"><?php echo $left_text ?></p>
                    <?php }
                    if ($right_text) { ?>
                        <p class="banner__bottom__right"><?php echo $right_text ?></p>
                    <?php } ?>
                </div>
            <?php } ?>
            <span class="banner__next">
                <?php echo file_get_contents(THEME_URI . '/dest/img/arrow-down.svg') ?>
            </span>
        </div>
    </div>
</section>