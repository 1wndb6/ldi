<?php

define('THEME_URI', get_template_directory_uri());
define('THEME_PATH', get_template_directory());

include 'inc/enqueue.php';
include 'inc/menu.php';
include 'inc/acf.php';

add_theme_support('custom-logo');

add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    $mimes['webp'] = 'image/webp';
    return $mimes;
});